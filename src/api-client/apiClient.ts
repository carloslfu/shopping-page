import productsData from "./products.json";

export type ProductData = typeof productsData[0];

export const apiClient = {
  getProducts({
    page = 0,
    pageSize = 20,
    filters = [] as string[],
  }): Promise<{ products: ProductData[]; hasMore: boolean }> {
    return new Promise((resolve) => {
      const filteredProducts =
        filters.length > 0
          ? productsData.filter((product) => filters.includes(product.category))
          : productsData;

      let result = filteredProducts.slice(
        page * pageSize,
        (page + 1) * pageSize,
      );

      setTimeout(() => {
        resolve({
          products: result,
          hasMore: (page + 1) * pageSize < filteredProducts.length,
        });
      }, 500);
    });
  },
};
