import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ProductData } from "../../api-client/apiClient";

interface ProductState {
  data: ProductData;
  amount: number;
}

interface ShoppingCartState {
  isOpen: boolean;
  products: Record<string, ProductState>;
}

const initialState: ShoppingCartState = {
  isOpen: false,
  products: {},
};

export const shoppingCartSlice = createSlice({
  name: "shoppingCart",
  initialState,
  reducers: {
    open(state) {
      state.isOpen = true;
    },
    close(state) {
      state.isOpen = false;
    },
    add(state, action: PayloadAction<ProductData>) {
      const productData = action.payload;
      const productId = productData.productId.value;

      if (state.products[productId]) {
        state.products[productId].amount++;
      } else {
        state.products[productId] = {
          data: productData,
          amount: 1,
        };
      }
    },
    remove(state, action: PayloadAction<ProductData>) {
      const productData = action.payload;
      const productId = productData.productId.value;

      if (state.products[productId]) {
        if (state.products[productId].amount > 1) {
          state.products[productId].amount--;
        } else {
          delete state.products[productId];
        }
      }
    },
  },
});

export const ShoppingCartActions = shoppingCartSlice.actions;

export const ShoppingCartReducer = shoppingCartSlice.reducer;
