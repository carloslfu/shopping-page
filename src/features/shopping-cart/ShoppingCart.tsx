import React from "react";
import styled from "styled-components";
import { useAppDispatch } from "../../app/hooks";
import { Icon } from "../../common/Icon";
import { ShoppingCartButton } from "../../common/ShoppingCartButton";
import { ShoppingCartActions } from "./shoppingCartSlice";

export function ShoppingCart() {
  return (
    <ShoppingCartContainer>
      <ShoppingCartHeader>
        <ShoppingCartCloseButton />

        <div>Your Cart</div>

        <ShoppingCartButton />
      </ShoppingCartHeader>
    </ShoppingCartContainer>
  );
}

const ShoppingCartContainer = styled.div`
  width: 373px;
  background-color: white;
  border-left: 1px solid ${({ theme }) => theme.color.greyLight};
`;

const ShoppingCartHeader = styled.div`
  height: 60px;
  padding: 0 8px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

function ShoppingCartCloseButton() {
  const dispatch = useAppDispatch();

  return (
    <StyledCloseButton onClick={() => dispatch(ShoppingCartActions.close())}>
      <Icon name="arrow_forward" color="primary" />
    </StyledCloseButton>
  );
}

const StyledCloseButton = styled.button`
  width: 35px;
  height: 35px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: none;
  border: none;
  cursor: pointer;
  border-radius: 50%;

  &:hover {
    background-color: ${({ theme }) => theme.color.greyLight};
  }
`;
