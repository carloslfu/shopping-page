import React, { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { apiClient, ProductData } from "../../api-client/apiClient";
import { Product } from "./Product";

const categories = [
  "Alcohol",
  "Bakery",
  "Dairy & Eggs",
  "Drinks",
  "Frozen",
  "Home & Health",
  "Meat, Fish, & Protein",
  "Pantry",
  "Pet Products",
  "Prepared",
  "Produce",
  "Snacks",
];

type ShoppingPageState = "idle" | "loading" | "loaded" | "error";

export function ShoppingPage() {
  const [state, setState] = useState<ShoppingPageState>("loading");
  const [products, setProducts] = useState<ProductData[]>([]);

  const [hasMore, setHasMore] = useState(false);
  const [page, setPage] = useState(0);
  const [loadMoreState, setLoadMoreState] = useState<
    "idle" | "loading" | "error"
  >("idle");
  const [filters, setFilters] = useState<Record<string, boolean>>({});

  const getProducts = useCallback(() => {
    setState("loading");

    apiClient
      .getProducts({
        filters: extractFilters(filters),
      })
      .then((result) => {
        setProducts(result.products);
        setHasMore(result.hasMore);
        setState("loaded");
      })
      .catch((error) => {
        setState("error");
      });
  }, [filters]);

  const loadMore = useCallback(() => {
    setLoadMoreState("loading");

    apiClient
      .getProducts({
        page: page + 1,
        filters: extractFilters(filters),
      })
      .then((result) => {
        setPage(page + 1);
        setProducts([...products, ...result.products]);
        setHasMore(result.hasMore);
        setLoadMoreState("idle");
      })
      .catch((error) => {
        setLoadMoreState("error");
      });
  }, [filters, page, products]);

  useEffect(() => {
    getProducts();
  }, [filters, getProducts]);

  return (
    <StyledContainer>
      <StyledFilterTitle>Shop by category</StyledFilterTitle>

      <StyledShoppingFilters>
        {categories.map((category) => (
          <StyledShoppingFilter
            key={category}
            isSelected={filters[category]}
            onClick={() =>
              setFilters((obj) => ({ ...obj, [category]: !obj[category] }))
            }
          >
            {category}
          </StyledShoppingFilter>
        ))}
      </StyledShoppingFilters>

      {state === "error" && (
        <div>
          We are having issues to get the products, please try again or contact
          support
          <button onClick={getProducts}>Try again</button>
        </div>
      )}

      {state === "loading" && <div>loading ...</div>}

      {state === "loaded" && (
        <StyledProductSection>
          <StyledProductGrid>
            {products.map((product) => (
              <Product key={product.productId.value} data={product} />
            ))}
          </StyledProductGrid>

          {loadMoreState === "loading" && <div>Loading ...</div>}

          {loadMoreState === "error" && (
            <div>
              Error loading more products
              <button onClick={loadMore}>Try again</button>
            </div>
          )}

          {hasMore && loadMoreState === "idle" && (
            <StyledLoadMoreButton onClick={loadMore}>
              Load more
            </StyledLoadMoreButton>
          )}
        </StyledProductSection>
      )}
    </StyledContainer>
  );
}

const StyledContainer = styled.div`
  padding: 0 100px 40px 100px;
`;

const StyledFilterTitle = styled.h1`
  margin: 0;
  padding: 0;
  margin-top: 30px;
  font-size: 12px;
`;

const StyledShoppingFilters = styled.div`
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  gap: 9px;
`;

const StyledShoppingFilter = styled.button<{ isSelected: boolean }>`
  padding: 6px 16px;
  border: 1px solid
    ${({ theme, isSelected }) =>
      isSelected ? theme.color.primary : theme.color.greyLight};
  border-radius: 32px;
  color: ${({ isSelected }) => (isSelected ? "white" : "inherit")};
  background-color: ${({ theme, isSelected }) =>
    isSelected ? theme.color.primary : "white"};
  cursor: pointer;

  &:hover {
    border: 1px solid ${({ theme }) => theme.color.primary};
  }
`;

const StyledProductSection = styled.div`
  margin-top: 49px;
`;

const StyledProductGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
`;

const StyledLoadMoreButton = styled.button`
  width: 100%;
  padding: 8px 16px;
  border-radius: 2px;
  background: none;
  border: none;
  color: ${({ theme }) => theme.color.primary};
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.color.greyLight};
  }
`;

function extractFilters(filters: Record<string, boolean>): string[] {
  return Object.entries(filters)
    .filter(([name, isSelected]) => isSelected)
    .map(([name]) => name);
}
