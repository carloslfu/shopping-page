import React from "react";
import styled from "styled-components";

import { ProductData } from "../../api-client/apiClient";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { Icon } from "../../common/Icon";
import { ShoppingCartActions } from "../shopping-cart/shoppingCartSlice";

export interface ProductProps {
  data: ProductData;
}

const currencyFormatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  currencyDisplay: "narrowSymbol",
});

export function Product({ data }: ProductProps) {
  const productState = useAppSelector(
    (state) => state.shoppingCart.products[data.productId.value],
  );
  const dispatch = useAppDispatch();

  return (
    <StyledProductContainer>
      <StyledProductImage
        src={data.imageUrl}
        alt={`product image for ${data.name}`}
      />
      <StyledProductName>{data.name}</StyledProductName>
      <StyledProductPrice>
        {currencyFormatter.format(data.price / 100)}
      </StyledProductPrice>
      <div>
        {productState ? (
          <StyledProductAmountContainer>
            <IconButton
              onClick={() => dispatch(ShoppingCartActions.remove(data))}
            >
              <Icon name="remove_circle_outline" color="grey" />
            </IconButton>
            <div>{productState.amount}</div>
            <IconButton onClick={() => dispatch(ShoppingCartActions.add(data))}>
              <Icon name="add_circle" color="primary" />
            </IconButton>
          </StyledProductAmountContainer>
        ) : (
          <StyledAddProductButton
            onClick={() => dispatch(ShoppingCartActions.add(data))}
          >
            <Icon name="add_circle_outline" color="primary" size="small" />
            <div style={{ marginLeft: 5 }}>Add to Cart</div>
          </StyledAddProductButton>
        )}
      </div>
    </StyledProductContainer>
  );
}

const StyledProductContainer = styled.div`
  width: 224px;
  min-height: 285px;
  margin-top: 20px;
  padding: 12px;

  &:hover {
    box-shadow: 0px 0px 0px 1px ${({ theme }) => theme.color.greyLight};
  }
`;

const StyledProductImage = styled.img`
  display: block;
  width: 100%;
  height: 154px;
`;

const StyledProductName = styled.p`
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-align: center;
  font-size: 14px;
  margin: 10px 0 10px 0;

  &:hover {
    display: block;
  }
`;

const StyledProductPrice = styled.div`
  text-align: center;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 14px;
`;

const StyledAddProductButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 36px;
  border: 1px solid ${({ theme }) => theme.color.greyLight};
  background: none;
  color: ${({ theme }) => theme.color.primary};
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.color.greyLight};
    transition: background-color ease 0.4s;
  }
`;

const StyledProductAmountContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid ${({ theme }) => theme.color.greyLight};
`;

const IconButton = styled.button`
  height: 36px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: none;
  cursor: pointer;
  border: none;
`;
