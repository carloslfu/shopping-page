import { DefaultTheme } from "styled-components";

export const theme: DefaultTheme = {
  color: {
    primary: "#38C2DE",
    secondary: "#EC6661",
    greyLight: "rgba(215, 215, 215, 0.5)",
    grey: "#777777",
  },
};
