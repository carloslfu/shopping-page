import React from "react";
import { Route, Routes } from "react-router-dom";
import styled from "styled-components";

import { Header } from "../common/Header";
import { ShoppingCart } from "../features/shopping-cart/ShoppingCart";
import { ShoppingPage } from "../features/shopping-page/ShoppingPage";
import { useAppSelector } from "./hooks";

export function App() {
  const isShoppingCartOpen = useAppSelector(
    (state) => state.shoppingCart.isOpen,
  );

  return (
    <StyledAppContainer>
      <StyledMainContainer isShoppingCartOpen={isShoppingCartOpen}>
        <Header />

        <Routes>
          <Route index element={<ShoppingPage />} />
          <Route path="/shopping" element={<ShoppingPage />} />
          <Route path="*" element={<div>404 not found</div>} />
        </Routes>
      </StyledMainContainer>

      {isShoppingCartOpen && <ShoppingCart />}
    </StyledAppContainer>
  );
}

const StyledAppContainer = styled.div`
  display: flex;
  height: 100%;
`;

interface MainContainerProps {
  isShoppingCartOpen: boolean;
}

const StyledMainContainer = styled.div<MainContainerProps>`
  width: ${({ isShoppingCartOpen }) =>
    isShoppingCartOpen ? "calc(100% - 373px)" : "100%"};
  height: 100%;
`;
