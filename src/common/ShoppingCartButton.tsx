import React from "react";
import styled from "styled-components";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { ShoppingCartActions } from "../features/shopping-cart/shoppingCartSlice";
import { Icon } from "./Icon";

export function ShoppingCartButton() {
  const numProducts = useAppSelector((state) =>
    Object.values(state.shoppingCart.products).reduce(
      (total, product) => total + product.amount,
      0,
    ),
  );
  const isOpen = useAppSelector((state) => state.shoppingCart.isOpen);
  const dispatch = useAppDispatch();

  return (
    <ShoppingCartIconContainer
      onClick={() => dispatch(ShoppingCartActions.open())}
      isOpen={isOpen}
    >
      {numProducts > 0 && !isOpen && (
        <StyledIconBadge>{numProducts}</StyledIconBadge>
      )}
      <Icon name="shopping_cart" color="grey" />
      <div>Cart</div>
    </ShoppingCartIconContainer>
  );
}

const ShoppingCartIconContainer = styled.button<{ isOpen: boolean }>`
  height: 100%;
  padding: 0 5px;
  position: relative;
  flex-direction: column;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  color: ${({ theme }) => theme.color.grey};
  background: none;
  border: none;
  cursor: pointer;

  &:hover {
    background-color: ${({ theme, isOpen }) =>
      isOpen ? "white" : theme.color.greyLight};
  }
`;

const StyledIconBadge = styled.span`
  position: absolute;
  top: 1px;
  right: 0px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 18px;
  height: 18px;
  background-color: ${({ theme }) => theme.color.secondary};
  border-radius: 50%;
  font-size: 12px;
  color: white;
  text-align: center;
`;
