import React from "react";
import styled from "styled-components";

import { useAppSelector } from "../app/hooks";
import { ShoppingCartButton } from "./ShoppingCartButton";

export function Header() {
  const isShoppingCartOpen = useAppSelector(
    (state) => state.shoppingCart.isOpen,
  );

  return (
    <StyledHeader>{!isShoppingCartOpen && <ShoppingCartButton />}</StyledHeader>
  );
}

const StyledHeader = styled.div`
  position: sticky;
  height: 60px;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0 30px;
  background-color: white;
  border-bottom: 1px solid ${({ theme }) => theme.color.greyLight};
`;
