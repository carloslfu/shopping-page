import React from "react";
import styled, { DefaultTheme } from "styled-components";

export interface IconProps {
  className?: string;
  name: string;
  color?: keyof DefaultTheme["color"];
  size?: "small" | "medium";
}

function BaseIcon({ className, name }: IconProps) {
  return <span className={"material-icons-outlined " + className}>{name}</span>;
}

const sizes = {
  small: 16,
  medium: 24,
};

export const Icon = styled(BaseIcon)`
  color: ${({ theme, color = "grey" }) => theme.color[color]};
  font-size: ${({ size = "medium" }) => sizes[size]}px;
`;
